import Data.Char
import Data.List

nbLettres :: String -> [(Char,Int)]
nbLettres str = map (\l@(x:xs) -> (x,length l)) . group . sort . map toLower . filter (isAlpha) $ str

separer :: Int -> String -> [String]
separer _ [] = error "empty"
separer _ [chr] = [[chr]]
separer n (chr:str) = (chr:(take (n-1) str)):(separer n str)

enumerer :: Int -> [String] -> [(String,Int)]
enumerer _ [] = error "empty"
enumerer n x = map (\l@(x:xs) -> (x,length l)) . group . sort . filter equalLength $ x
  where equalLength xs = length xs == n
