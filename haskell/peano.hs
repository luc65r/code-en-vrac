{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE UnicodeSyntax #-}

class Eq a where
    (≡) ∷ a → a → 𝔹

    (≠) ∷ a → a → 𝔹
    a ≠ b = (¬) (a ≡ b)

    (≥) ∷ a → a → 𝔹

    (>) ∷ a → a → 𝔹
    a > b = (¬) (b ≥ a)

    (≤) ∷ a → a → 𝔹
    a ≤ b = b ≥ a

    (<) ∷ a → a → 𝔹
    a < b = (¬) (a ≥ b)


class Num a where
    (+) ∷ a → a → a
    (-) ∷ a → a → a
    (×) ∷ a → a → a

data 𝔹 = False | True

instance Eq 𝔹 where
    True  ≡ True  = True
    False ≡ False = True
    _     ≡ _     = False

    False ≥ True = False
    _     ≥ _    = True

(¬) ∷ 𝔹 → 𝔹
(¬) True  = False
(¬) False = True

data ℕ = Zero | Succ ℕ

instance Eq ℕ where
    Zero   ≡ Zero   = True
    Succ a ≡ Succ b = a ≡ b
    _      ≡ _      = False

    a      ≥ Zero   = True
    Succ a ≥ Succ b = a ≥ b
    _      ≥ _      = False

instance Num ℕ where
    Zero     + b = b
    (Succ a) + b = Succ (a + b)

    a        - Zero     = a
    (Succ a) - (Succ b) = a - b

    Zero     × _ = Zero
    (Succ a) × b = (a × b) + b

(∘) ∷ (b → c) → (a → b) → a → c
f ∘ g = \x → f (g x)

(∞) ∷ ℕ
(∞) = Succ (∞)
