module Grid
    ( Grid
    , Square
    , Coords
    , Player(..)
    , inv
    , emptyGrid
    , validGrid
    , place
    , freePlace
    , placeValue
    ) where

import Data.List (elemIndices)

type Grid = [[Player]]

data Square = Square { coords :: Coords
                     , value :: Player
                     } deriving (Eq, Show, Read)

type Coords = (Int, Int)

data Player = Empty | X | O deriving (Eq, Read)

instance Show Player where
    show Empty = " "
    show X     = "X"
    show O     = "O"

-- Changes player
inv :: Player -> Player
inv p
    | p == Empty = Empty
    | p == X     = O
    | p == O     = X

-- Makes an empty m columns n lines grid
emptyGrid :: Int -> Int -> Grid
emptyGrid m n
    | n < 1 || m < 1 = error "invalid size"
    | otherwise = take n . repeat . take m . repeat $ Empty

-- Verifies if the grid is valid (non null rectangle)
validGrid :: Grid -> Bool
validGrid g
    | lenm < 1 || lenn < 1 = False
    | otherwise = all (== lenm) [length m | m <- g]
    where lenm = length (g !! 0)
          lenn = length g

-- Places a square on a grid
place :: Square -> Grid -> Grid
place Square {coords = (m,n), value = v} g
    | not $ validPlace (m,n) g = error "invalid coordinates"
    | otherwise = take (n - 1) g ++ 
                  [take (m - 1) gs ++ [v] ++ drop m gs]
                                            ++ drop n g
    where gs = g !! (n - 1)

-- Verifies if the place is free
freePlace :: Coords -> Grid -> Bool
freePlace (m,n) g
    | not $ validPlace (m,n) g = error "invalid coordinates"
    | placeValue (m,n) g == Empty = True
    | otherwise = False

-- Verifies if the place is valid
validPlace :: Coords -> Grid -> Bool
validPlace (m,n) g
    | not $ validGrid g = error "invalid grid"
    | m < 1 || n < 1 || m > length (g !! 0) || n > length g = False
    | otherwise = True

-- Returns the value of a place
placeValue :: Coords -> Grid -> Player
placeValue (m,n) g
    | not $ validPlace (m,n) g = error "invalid coordinates"
    | otherwise = (g !! (n - 1)) !! (m - 1)

-- Returns all the positions a player has
allPos :: Grid -> Player -> [Coords]
allPos [] _ = []
allPos g@(x:xs) p = (zip indices $ repeat $ length g) ++ allPos xs p
    where indices = map (+1) $ elemIndices p x

-- Verifies if a player has won
win :: Grid -> Player -> Bool
win _ Empty = error "invalid player"
win g p
    | not $ validGrid g = error "invalid grid"
