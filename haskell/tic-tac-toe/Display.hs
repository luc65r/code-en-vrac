module Display
    ( display
    , displayGrid
    ) where

import Grid

display :: [String] -> String
display [] = error "invalid size"
display [x] = x
display (x:xs) = x ++ "\n" ++ display xs

displayGrid :: Grid -> [String]
displayGrid g
    | not $ validGrid g = error "invalid grid"
    | lenn == 1 = [topGrid lenm, valGrid (g !! 0), botGrid lenm]
    | otherwise = [topGrid lenm, valGrid (g !! 0), midGrid lenm] ++ xs
    where lenm = length (g !! 0)
          lenn = length g
          (_:xs) = displayGrid (tail g)

topGrid :: Int -> String
topGrid n
    | n < 1 = error "invalid size"
    | n == 1 = "┌───┐"
    | otherwise = "┌───┬" ++ xs
    where (_:xs) = topGrid (n - 1)

midGrid :: Int -> String
midGrid n
    | n < 1 = error "invalid size"
    | n == 1 = "├───┤"
    | otherwise = "├───┼" ++ xs
    where (_:xs) = midGrid (n - 1)

botGrid :: Int -> String
botGrid n
    | n < 1 = error "invalid size"
    | n == 1 = "└───┘"
    | otherwise = "└───┴" ++ xs
    where (_:xs) = botGrid (n - 1)

valGrid :: [Player] -> String
valGrid [] = error "invalid size"
valGrid [x] = "│ " ++ show x ++ " │"
valGrid (x:xs) = "│ " ++ show x ++ " " ++ valGrid xs
