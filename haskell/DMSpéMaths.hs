rouge :: (Integral a) => (a,a) -> Bool
rouge (_,0) = error "division par 0"
rouge (a,b)
    | b' == 1   = if odd a' then True else False
    | odd q     = not $ rouge (b',r)
    | otherwise = rouge (b',r)
    where (a',b') = (div a $ gcd a b, div b $ gcd a b)
          (q,r)   = (div a' b', mod a' b')
