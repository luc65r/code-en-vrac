fn main() {
    const MAX: usize = 10_000_000;
    let mut primes: Box<[u8]> = Box::new([1; MAX + 1]);
    primes[0] = 0;
    primes[1] = 0;
    println!("{}", primes[MAX]);
    /* for i in 0..MAX {
        println!("{}", primes[i]);
    } */
}
