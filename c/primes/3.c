#include <aio.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include <string.h>

#define FILE_NAME "primes.txt"
#define BUFFER 2048000 /* 2 MB */

#define llu uint_fast64_t

#define TIME(func, ...) { \
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start); \
    func(__VA_ARGS__); \
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop); \
    print_done(&start, &stop); \
}

void init_array(llu bytes, uint8_t *primes);
void elim_np(llu n, uint8_t *primes);
void write_primes(llu n, uint8_t *primes);

void
write_buffer(const struct aiocb **aiolist,
             struct aiocb *op,
             int fd, llu offset,
             uint8_t *buffer, llu size);

void print_done(struct timespec *start, struct timespec *stop);

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Please supply one argument\n");
        return 1;
    }

    struct timespec start, stop;

    /* Read number given in arguments */
    llu n = strtoull(argv[1], NULL, 10);

    /* Bytes to allocate for n + 1 bits */
    llu alloc = n / 8 + 1;

    /* Allocate the array */
    uint8_t *primes = malloc(alloc);

    printf("Initializing %llu bytes of memory...\n", alloc);
    TIME(init_array, alloc, primes);

    printf("Eliminating non-primes...\n");
    TIME(elim_np, n, primes);

    printf("Writing primes to %s...\n", FILE_NAME);
    TIME(write_primes, n, primes);

    printf("Freeing memory...\n");
    TIME(free, primes);

    printf("Done!\n");
    return 0;
}

void init_array(llu bytes, uint8_t *primes) {
    /* Set 0, 1 and all evens to 0; 2, 3 and all odds to 1 */
    *primes = 0b00110101;
    memset(primes + 1, 0b01010101, bytes - 1);
}

void elim_np(llu n, uint8_t *primes) {
    /* Eliminate all non primes in the array by setting them to 0
     * using the sieve of Eratosthenes */
    for (llu i = 3; i <= sqrt(n); i += 2) {
        if (!(primes[i / 8] >> 7 - i % 8 & 1))
            continue;
        for (llu j = 3; ; j += 2) {
            llu m = i * j;
            if (m > n)
                break;
            primes[m / 8] &= 0xFF ^ (1 << 7 - m % 8);
        }
    }
}

void write_primes(llu n, uint8_t *primes) {
    /* Allocate the buffers */
    uint8_t *buffers[2];
    buffers[0] = malloc(BUFFER); /* First buffer */
    buffers[1] = malloc(BUFFER); /* Second buffer */
    int buffer = 0; /* Buffer we are filling */
    llu b = 0; /* Position in buffer */

    uint8_t *num = malloc(20); /* Buffer to write the number */

    struct aiocb op = { 0 };
    const struct aiocb *aiolist[1] = { NULL }; /* List of aio jobs */

    FILE *file;
    file = fopen(FILE_NAME, "w");
    int fd = fileno(file); /* File descriptor of the file */
    llu offset = 0; /* Position in the file */

    /* Fill a buffer while the other is beeing written to the file
     * so the file is continuously beeing written */
    for (llu i = 2; i <= n; i++) {
        if (primes[i / 8] >> 7 - i % 8 & 1) {
            int m = 0;
            /* Decompose the number into ascii digits (reversed) */
            for (llu j = i; j > 0; j /= 10) {
                num[m] = j % 10 + '0';
                m++;
            }

            /* If the buffer is to empty to add the number, write it. */
            if (b + m > BUFFER) {
                write_buffer(aiolist, &op, fd, offset, buffers[buffer], b);

                offset += b;
                buffer = !buffer; /* Switch to the other buffer */
                b = 0;
            }

            /* Write number in the buffer */
            while (m > 0) {
                m--;
                buffers[buffer][b] = num[m];
                b++;
            }
            buffers[buffer][b] = '\n';
            b++;
        }
    }

    write_buffer(aiolist, &op, fd, offset, buffers[buffer], b);
    aio_suspend(aiolist, 1, NULL);

    /* Free memory */
    free(buffers[0]); free(buffers[1]);
    free(num);
}

void
write_buffer(const struct aiocb **aiolist,
             struct aiocb *op,
             int fd, llu offset,
             uint8_t *buffer, llu size)
{
    aio_suspend(aiolist, 1, NULL);

    op->aio_fildes = fd;
    op->aio_offset = offset;
    op->aio_buf = buffer;
    op->aio_nbytes = size;

    aio_write(op);
    aiolist[0] = op;
}

void print_done(struct timespec *start, struct timespec *stop) {
    double secs = (stop->tv_sec - start->tv_sec) + (stop->tv_nsec - start->tv_nsec) / 1e9;
    const char *si[] = { "s", "ms", "µs" };
    int s = 0;
    for (; secs < 1 && s < 2; s++) secs *= 1e3;
    printf("Done in %.2lf %s.\n\n", secs, si[s]);
}
