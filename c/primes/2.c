#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("Please supply one argument");
		return 1;
	}

	unsigned long long int n = strtoull(argv[1], NULL, 10);

	char *primes = NULL;
	primes = malloc((n + 1) * sizeof(char));

	primes[0] = 0;
	primes[1] = 0;
	for (unsigned long long int i = 2; i <= n; i++) {
		primes[i] = 1;
	}
	
	for (unsigned long long int i = 2; i <= sqrt(n); i++) {
		if (primes[i] == 0)
			continue;
		for (unsigned long long int j = 2; j < n; j++) {
			unsigned long long int m = i * j;
			if (m > n)
				break;
			primes[m] = 0;
		}
	}
	
	printf("Writing to disk...\n");
	FILE *file = NULL;
	file = fopen("primes.txt", "w");

	if (file != NULL) {
		for (unsigned long long int i = 0; i <= n; i++) {
			if (primes[i] == 1)
				fprintf(file, "%llu\n", i);
		}
		return 0;
	} else {
		printf("Can't open file!\n");
		return 1;
	}
}
