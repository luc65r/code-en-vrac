#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {
	unsigned long long n = 10000000;
	FILE *file = NULL;
	file = fopen("primes.txt", "w");
	if (file != NULL) {
		for (unsigned long long i = 2; i <= n; i++) {
			char p = 1;
			for (unsigned long long j = 2; j < sqrt(i); j++) {
				if (i % j == 0) {
					p = 0;
					break;
				}
			}
			if (p == 1)
				fprintf(file, "%lld\n", i);
		}
		fclose(file);
		return 0;
	} else {
		printf("Can't open file!\n");
		return 1;
	}
}
