#Ifndef CLI
#define CLI

void cli(unsigned char grille[3][3], unsigned char *joueur, unsigned int score[]);
void afficher(unsigned char grille[3][3], unsigned int score[]);
unsigned char demander(unsigned char grille[3][3], unsigned char *joueur);

#endif
