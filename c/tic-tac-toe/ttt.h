#ifndef TTT
#define TTT

typedef struct Coords Coords;
struct Coords {
	int m;
	int n;
};

typedef enum Player Player;
enum Player {
	EMPTY, X, O
};

void newGame(Player grid[3][3], Player *player);

void put(Player grid[3][3], Coords place, Player *player);

int full(Player grid[m][n]);

int checkGrid(Player grid[3][3]);

int check(Player a, Player b, Player c);

#endif
