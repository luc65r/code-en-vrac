#include <stdio.h>
#include <stdlib.h>
#include "cli.h"
#include "ttt.h"

int main(int argc, char *argv[]) {
    Player grid[3][3] = {EMPTY};
    Player player = X;
    unsigned int score[2] = {0};
    cli(grid, &player, score);
    return 0;
}
