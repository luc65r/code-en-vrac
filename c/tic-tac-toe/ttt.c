#include "ttt.h"

void newGame(Player grid[3][3], Player *player) {
    for (int m = 0 ; m < 3 ; m++) {
        for (int n = 0 ; n < 3 ; n++) {
            grid[m][n] = EMPTY;
        }
    }
    *player = X;
}

void put(Player grid[3][3], Coords place, Player *player) {
    grid[place.m][place.n] = *player;
}

int full(Player grid[m][n]) {
    for (int m = 0 ; m < 3 ; m++) {
        for (int n = 0 ; n < 3 ; n++) {
            if (grid[m][n] == EMPTY)
                return 0;
        }
    }
    return 1;
}

int checkGrid(Player grid[3][3]) {
    for (int i = 0 ; i < 3 ; i++) {
        if (check(grid[i][0], grid[i][1], grid[i][2]))
            return 1;
        if (check(grid[0][i], grid[1][i], grid[2][i]))
            return 1;
    }
    if (check(grid[0][0], grid[1][1], grid[2][2]))
        return 1;
    if (check(grid[0][2], grid[1][1], grid[2][0]))
        return 1;
    return 0;
}

int check(Player a, Player b, Player c) {
    if (a == EMPTY)
        return 0;
    if (a == b && b == c)
        return 1;
    return 0;
}

