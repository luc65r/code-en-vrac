#include <stdio.h>
#include <stdlib.h>
#include "cli.h"
#include "ttt.h"

void cli(Player grid[3][3], Player *player, unsigned int score[]) {
    while (1) {
        display(grid, score);

        int invalid;
        do {
            invalid = ask(grid, *player);
        } while (invalid);

        display(grid, score);

        if (checkGrid(grid)) {
            newGame(grid, *player);
		} else if (full(grid)) {
			newGame(grid, *player);
		}
    }
}

void display(Player grid[3][3], unsigned int score[]) {
    printf("┌───┬───┬───┐\n│ %c │ %c │ %c │\n├───┼───┼───┤   X : %d\n│ %c │ %c │ %c │\n├───┼───┼───┤   O : %d\n│ %c │ %c │ %c │\n└───┴───┴───┘\n", toChar(grid[0][0]), toChar(grid[1][0]), toChar(grid[2][0]), score[0], toChar(grid[0][1]), toChar(grid[1][1]), toChar(grid[2][1]), score[1], toChar(grid[0][2]), toChar(grid[1][2]), toChar(grid[2][2]));
}

int askPlayer(grid[3][3], Player *player) {
    char input = ' ';
    printf("%c : ", *player);
    scanf("%c", &input);
	switch (input) {
		case '1':
			put(grid, {0, 2}, *player);
			return 0;
		case '2':
			put(grid, {1, 2}, *player);
			return 0;
		case '3':
			put(grid, {2, 2}, *player);
			return 0;
		case '4':
			put(grid, {0, 1}, *player);
			return 0;
		case '5':
			put(grid, {1, 1}, *player);
			return 0;
		case '6':
			put(grid, {2, 1}, *player);
			return 0;
		case '7':
			put(grid, {0, 0}, *player);
			return 0;
		case '8':
			put(grid, {1, 0}, *player);
			return 0;
		case '9':
			put(grid, {2, 0}, *player);
			return 0;
		default:
			return 1;
	}
}

char toChar(Player player) {
	switch (player) {
		case EMPTY:
			return ' ';
			break;
		case X:
			return 'X';
			break;
		case O:
			return 'O';
			break;
	}
}

