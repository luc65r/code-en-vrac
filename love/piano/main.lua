function love.load()
	math.randomseed(os.time())
	love.window.setFullscreen(true)
	love.mouse.setVisible(false)

	note = {
		a = love.audio.newSource("1.wav", "static")
	}
	
	font = {
		score = love.graphics.newFont(64)
	}

	display = {
		i = false,
	        w = love.graphics.getHeight() / 2,
	        h = love.graphics.getHeight()
	}

	init()
end

function love.keypressed(k)
	if k == "q" or k == "escape" then
		love.event.quit()
	end
	if k == "i" then
		interface = not interface
	end
	if k == "d" then
		verify(1)
	end
	if k == "f" then
		verify(2)
	end
	if k == "j" then
		verify(3)
	end
	if k == "k" then
		verify(4)
	end
end	

function love.update(dt)
	if not pause then
		for i = 1, tiles.n do
			tiles[i].y = tiles[i].y + tiles.v * dt
			if tiles[i].y > display.h then
				lost()
			end
		end
	end
end

function love.draw()
	for i = 1, tiles.n do
		love.graphics.rectangle("fill", tiles[i].x(), tiles[i].y, tiles.w, tiles.h)
	end
	for i = 0, 4 do
		love.graphics.rectangle("fill", tiles.x0 + tiles.w * i, 0, 1, display.h)
	end
	if interface then
		love.graphics.print("FPS : " .. love.timer.getFPS(), 50, 50)
	end
	love.graphics.printf({{.5, .5, .5}, score}, font.score, 0, 50, 1920, 'center')
	
end

function init()
	pause = true
	oldscore = score
	score = 0

	tiles = {
		n = 10,
		v = 500,
		w = display.w / 4
	}

	tiles.h = display.h * 2 / tiles.n
	tiles.x0 = (love.graphics.getWidth() - display.w) / 2

	for i = 1, tiles.n do
		defTile(i)
	end
end

function defTile(n)
	local rand = math.random(1, 4)
	if n ~= 1 then
		while rand == tiles[n-1].pos do
			rand = math.random(1, 4)
		end
	end
	tiles[n] = {
		y = n == 1 and 0 or tiles[n-1].y - tiles.h,
		pos = rand,
		x = function() return tiles.x0 + (tiles[n].pos - 1) * tiles.w end
	}
end

function verify(n)
	if tiles[1].pos == n then
		replace()
		pause = false
		note.a:stop()
		note.a:play()
		tiles.v = tiles.v + 2500 / tiles.v
		score = score + 1
	else
		lost()
	end
end

function replace()
	for i = 1, tiles.n - 1 do
		tiles[i] = {
			y = tiles[i+1].y,
			pos = tiles[i+1].pos,
			x = function() return tiles.x0 + (tiles[i].pos - 1) * tiles.w end
		}
	end
	defTile(tiles.n)
end

function lost()
	init()
end
