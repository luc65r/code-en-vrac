raquetteD = {}
raquetteG = {}
balle = {}
scoreD = {}
scoreG = {}
filet = {}
jeu = {}

function love.load()
	love.window.setFullscreen(true)
	love.mouse.setVisible(false)
	love.graphics.setFont(love.graphics.newFont(64))	

	interface = false
	
	dcD = false -- Dernier coup est fait par le joueur de droite

	-- Taille de l'écran
	width = love.graphics.getWidth()
	height = love.graphics.getHeight()

	-- Taille du jeu
	if width > height then
		jeu.taille = height
	else
		jeu.taille = width
	end
	jeu.xmid = math.floor(width / 2)
	jeu.xmin = math.floor(jeu.xmid - jeu.taille / 2)
	jeu.xmax = math.floor(jeu.xmid + jeu.taille / 2)
	jeu.ymid = math.floor(height / 2)
	jeu.ymin = math.floor(jeu.ymid - jeu.taille / 2)
	jeu.ymax = math.floor(jeu.ymid + jeu.taille / 2)
	
	balle.w = math.floor(jeu.taille / 50) -- 1/50 de la taille du jeu
	balle.x = math.floor(jeu.xmid - balle.w / 2)
	balle.y = math.floor(jeu.ymid - balle.w / 2)
	balle.vitesse = 500 -- En pixels par seconde
	balle.acceleration = 1.05
	balle.direction = 0 -- Angle orienté en radians
	
	raquetteD.w = balle.w
	raquetteD.h = math.floor(jeu.taille / 20)
	raquetteD.x = jeu.xmax - raquetteD.w
	raquetteD.y = math.floor(jeu.ymid - raquetteD.h / 2)
	raquetteD.vitesse = 500

	raquetteG.w = raquetteD.w
	raquetteG.h = raquetteD.h
	raquetteG.x = jeu.xmin
	raquetteG.y = math.floor(jeu.ymid - raquetteG.h / 2)
	raquetteG.vitesse = raquetteD.vitesse

	filet.w = math.floor(jeu.taille / 100)
	filet.x = math.floor(jeu.xmid - filet.w / 2)
	filet.u = balle.w

	scoreG.v = 0
	scoreD.v = 0
	scoreG.x = math.floor((jeu.xmin + jeu.xmid) / 2)
	scoreD.x = math.floor((jeu.xmid + jeu.xmax) / 2)
	scoreG.y = 50
	scoreD.y = 50
end

function love.keypressed(k)
	if k == "q" or k == "escape" then
		love.event.quit()
	end
	if k == "i" then
		interface = not interface
	end
end	

function love.update(dt)
	-- Contrôle des raquettes
	if love.keyboard.isDown("up") then
		raquetteD.y = raquetteD.y - raquetteD.vitesse * dt
	end
	if love.keyboard.isDown("down") then
		raquetteD.y = raquetteD.y + raquetteD.vitesse * dt
	end
	if love.keyboard.isDown("z") then
		raquetteG.y = raquetteG.y - raquetteG.vitesse * dt
	end
	if love.keyboard.isDown("s") then
		raquetteG.y = raquetteG.y + raquetteG.vitesse * dt
	end
	-- Empêche les raquettes de sortir du jeu
	if raquetteD.y < jeu.ymin then
		raquetteD.y = jeu.ymin 
	end
	if raquetteD.y > jeu.ymax - raquetteD.h then
		raquetteD.y = jeu.ymax - raquetteD.h
	end
	if raquetteG.y < jeu.ymin then
		raquetteG.y = jeu.ymin 
	end
	if raquetteG.y > jeu.ymax - raquetteG.h then
		raquetteG.y = jeu.ymax - raquetteG.h
	end
	
	-- La balle
	balle.x = balle.x + math.cos(balle.direction) * balle.vitesse * dt
	balle.y = balle.y - math.sin(balle.direction) * balle.vitesse * dt
	-- Collision avec les raquettes
	if not dcD and balle.x + balle.w >= raquetteD.x and balle.y >= raquetteD.y - balle.w and balle.y <= raquetteD.y + raquetteD.h then
		dy = balle.y - raquetteD.y 
		overshoot = (balle.x + balle.w - raquetteD.x) / math.cos(balle.direction)
		balle.x = balle.x - math.cos(balle.direction) * overshoot
		balle.y = balle.y + math.sin(balle.direction) * overshoot
		balle.direction = (- balle.direction + ((2*(dy + balle.w)/(raquetteD.h + balle.w) + 3) * math.pi / 4)) % (math.pi * 2)
		balle.x = balle.x + math.cos(balle.direction) * overshoot * balle.acceleration
		balle.y = balle.y - math.sin(balle.direction) * overshoot * balle.acceleration
		balle.vitesse = balle.vitesse * balle.acceleration
		dcD = true
	end
	if dcD and balle.x <= raquetteG.x + raquetteG.w and balle.y >= raquetteG.y - balle.w and balle.y <= raquetteG.y + raquetteG.h then
		dy = balle.y - raquetteG.y
		overshoot = (balle.x - raquetteG.x- raquetteG.w) / math.cos(balle.direction)
		balle.x = balle.x - math.cos(balle.direction) * overshoot
		balle.y = balle.y + math.sin(balle.direction) * overshoot
		balle.direction = (- balle.direction - ((2*(dy + balle.w)/(raquetteG.h + balle.w) + 3) * math.pi / 4)) % (math.pi * 2)
		balle.x = balle.x + math.cos(balle.direction) * overshoot * balle.acceleration
		balle.y = balle.y - math.sin(balle.direction) * overshoot * balle.acceleration
		balle.vitesse = balle.vitesse * balle.acceleration
		dcD = false
	end
	-- Collision avec les murs
	if balle.y < jeu.ymin then
		overshoot = (jeu.ymin - balle.y) / math.sin(balle.direction)
		balle.x = balle.x - math.cos(balle.direction) * overshoot
		balle.y = balle.y + math.sin(balle.direction) * overshoot
		balle.direction = (- balle.direction) % (math.pi * 2)
		balle.x = balle.x + math.cos(balle.direction) * overshoot
		balle.y = balle.y - math.sin(balle.direction) * overshoot
	end
	if balle.y > jeu.ymax - balle.w then
		overshoot = (jeu.ymax - balle.w - balle.y) / math.sin(balle.direction)
		balle.x = balle.x - math.cos(balle.direction) * overshoot
		balle.y = balle.y + math.sin(balle.direction) * overshoot
		balle.direction = (- balle.direction) % (math.pi * 2)
		balle.x = balle.x + math.cos(balle.direction) * overshoot
		balle.y = balle.y - math.sin(balle.direction) * overshoot
	end
	-- Sortie du jeu
	if balle.x < jeu.xmin then
		balle.direction = 0
		balle.x = math.floor(jeu.xmid - balle.w / 2)
		balle.y = math.floor(jeu.ymid - balle.w / 2)
		scoreD.v = scoreD.v + 1
		dcD = false -- On autorise le joueur de droite à toucher la balle
		raquetteD.x = jeu.xmax - raquetteD.w
		raquetteD.y = math.floor(jeu.ymid - raquetteD.h / 2)
		raquetteG.x = jeu.xmin
		raquetteG.y = math.floor(jeu.ymid - raquetteG.h / 2)
		balle.vitesse = 500
	end
	if balle.x > jeu.xmax - balle.w then
		balle.direction = math.pi
		balle.x = math.floor(jeu.xmid - balle.w / 2)
		balle.y = math.floor(jeu.ymid - balle.w / 2)
		scoreG.v = scoreG.v + 1
		dcD = true -- On autorise le joueur de gauche à toucher la balle
		raquetteD.x = jeu.xmax - raquetteD.w
		raquetteD.y = math.floor(jeu.ymid - raquetteD.h / 2)
		raquetteG.x = jeu.xmin
		raquetteG.y = math.floor(jeu.ymid - raquetteG.h / 2)
		balle.vitesse = 500
	end
end

function love.draw()
	-- Le filet
	for i=jeu.ymin,jeu.ymax,filet.u*2 do
		love.graphics.rectangle("fill", filet.x, i, filet.w, filet.u)
	end

	-- Les raquettes
	love.graphics.rectangle("fill", raquetteD.x, raquetteD.y, raquetteD.w, raquetteD.h)
	love.graphics.rectangle("fill", raquetteG.x, raquetteG.y, raquetteG.w, raquetteG.h)

	-- La balle
	love.graphics.rectangle("fill", math.floor(balle.x), math.floor(balle.y), balle.w, balle.w)
	
	-- Le score
	love.graphics.print(tostring(scoreG.v), scoreG.x, scoreG.y)
	love.graphics.print(tostring(scoreD.v), scoreD.x, scoreD.y)

	if interface then
		love.graphics.print("FPS : " .. love.timer.getFPS(), 50, 50)
	end
end
