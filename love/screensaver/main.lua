logo = {}

function love.load()
	love.window.setFullscreen(true)
	love.mouse.setVisible(false)

	interface = false
	
	width = love.graphics.getWidth()
	height = love.graphics.getHeight()
	
	images = love.filesystem.getDirectoryItems("logos")
	logo.numero = 1
	logo.image = love.graphics.newImage("logos/" .. images[logo.numero])
	
	-- Emplacement et taille du logo
	logo.x = 0
	logo.y = 0
	logo.w = logo.image:getWidth()
	logo.h = logo.image:getHeight()
	logo.scale = 1

	logo.vitesse = 500 -- Pixels par seconde

	-- Direction du logo
	logo.droite = true
	logo.bas = true
	
	dark = true
end

function love.keypressed(k)
	if k == "q" or k == "escape" then
		love.event.quit()
	end
	if k == "i" then
		interface = not interface
	end
	if k == "d" then
		dark = not dark
	end
	if k == "n" and logo.numero < table.getn(images) then
		logo.numero = logo.numero + 1
		logo.image = love.graphics.newImage("logos/" .. images[logo.numero])
		logo.w = logo.image:getWidth()
		logo.h = logo.image:getHeight()
	end
	if k == "p" and logo.numero > 1 then
		logo.numero = logo.numero - 1
		logo.image = love.graphics.newImage("logos/" .. images[logo.numero])
		logo.w = logo.image:getWidth()
		logo.h = logo.image:getHeight()
	end
end	

function love.update(dt)
	
	if love.keyboard.isDown("up") then
		logo.vitesse = logo.vitesse + 1000 * dt
	end
	if love.keyboard.isDown("down") then
		logo.vitesse = logo.vitesse - 1000 * dt
	end
	if love.keyboard.isDown("right") then
		logo.vitesse = logo.vitesse + 100 * dt
	end
	if love.keyboard.isDown("left") then
		logo.vitesse = logo.vitesse - 100 * dt
	end
	if logo.vitesse < 0 then
		logo.vitesse = 0
	end
	
	if love.keyboard.isDown("a") and (logo.w * logo.scale * 0.5) < width and (logo.h * logo.scale * 0.5) < height then
		logo.scale = logo.scale * 1.01
		logo.w = logo.image:getWidth() * logo.scale
		logo.h = logo.image:getHeight() * logo.scale
	end
	if love.keyboard.isDown("z") then
		logo.scale = logo.scale * 0.98
		logo.w = logo.image:getWidth() * logo.scale
		logo.h = logo.image:getHeight() * logo.scale
	end

	if logo.droite then
		logo.x = logo.x + logo.vitesse * dt
	else
		logo.x = logo.x - logo.vitesse * dt
	end
	if logo.bas then
		logo.y = logo.y + logo.vitesse * dt
	else
		logo.y = logo.y - logo.vitesse * dt
	end

	if logo.x < 0 then
		logo.x = - logo.x
		logo.droite = true
	elseif logo.x > (width - logo.w) then
		logo.x = (width - logo.w) * 2 - logo.x
		logo.droite = false
	end
	if logo.y < 0 then
		logo.y = - logo.y
		logo.bas = true
	elseif logo.y > (height - logo.h) then
		logo.y = (height - logo.h) * 2 - logo.y
		logo.bas = false
	end

end

function love.draw()
	if dark then
		love.graphics.setBackgroundColor(0, 0, 0)
	else
		love.graphics.setBackgroundColor(1, 1, 1)
	end
	
	-- Dessiner le logo
	love.graphics.setColor(1, 1, 1)
	love.graphics.draw(logo.image, logo.x, logo.y, 0, logo.scale, logo.scale)

	if interface then
		if dark then
			love.graphics.setColor(1, 1, 1)
		else
			love.graphics.setColor(0, 0, 0)
		end
		love.graphics.print("FPS : " .. love.timer.getFPS(), 50, 50)
		love.graphics.print("Vitesse : " .. math.floor(logo.vitesse), 50, 70)
		love.graphics.print("Taille : " .. math.floor(logo.w * logo.scale * 0.5) .. "x" .. math.floor(logo.h * logo.scale * 0.5), 50, 90)
	end
end
