#!/usr/bin/env python3

grille = [[8, 0, 1, 7, 9, 4, 0, 0, 0],
          [4, 0, 2, 0, 0, 5, 0, 0, 8],
          [0, 9, 0, 0, 0, 2, 1, 5, 0],
          [5, 0, 8, 4, 7, 0, 0, 0, 0],
          [0, 0, 6, 0, 5, 0, 2, 0, 0],
          [0, 0, 0, 0, 6, 8, 5, 0, 3],
          [0, 6, 3, 5, 0, 0, 0, 2, 0],
          [2, 0, 0, 8, 0, 0, 4, 0, 9],
          [0, 0, 0, 9, 2, 6, 3, 0, 5]]

grille_temporaire = [[0, 0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0, 0]]

fini = False

def resoudre_facile():
    """Fonction qui parcourt la grille et place les chiffres possibles
    dans la grille temporaire"""
    for y in range(0, 9):
        for x in range(0, 9):
            if grille[y][x] == 0:
                grille_temporaire[y][x] = chiffres_possibles(x, y)

def placer_chiffres():
    """Fonction qui places les chiffes sûrs dans la grille"""
    for y in range(0, 9):
        for x in range(0, 9):
            if isinstance(grille_temporaire[y][x], set) \
            and len(grille_temporaire[y][x]) == 1:
                grille[y][x] = grille_temporaire[y][x].pop()
                grille_temporaire[y][x] = 0

def chiffres_possibles(x, y):
    """Fonction qui détermine les chiffres possibles en fonction des
    autres cases de la même ligne, de la même colonne ou du même
    carré."""
    case = {1, 2, 3, 4, 5, 6, 7, 8, 9}
    for i in range(0, 9):
        case.discard(grille[y][i])
        case.discard(grille[i][x])
        case.discard(grille[carre_y(y, i)][carre_x(x, i)])
        if len(case) == 0:
            print("Erreur !")
    return case

def verifier(liste):
    """Fonction qui sert à vérifier si la liste de 9 nombres contient
    1, 2, 3, 4, 5, 6, 7, 8 et 9."""
    if len(liste) != 9:
        raise ListError("La liste ne fait pas 9 de longueur.")
    if liste.sort == [1, 2, 3, 4, 5, 6, 7, 8, 9]:
        return True
    return False

def carre_x(x, n):
    """Fonction qui retourne l'abscisse de la case numéro n dans
    le carré où se trouve la case d'abscisse x."""
    return (x // 3) * 3 + n % 3

def carre_y(y, n):
    """Fonction qui retourne l'abscisse de la case numéro n dans
    le carré où se trouve la case d'abscisse y."""
    return (y // 3) * 3 + n % 3

while not fini:
    resoudre_facile()
    placer_chiffres()

#    for sublist in grille:
#        if 0 not in sublist: fini = True

    for i in range(0, 9):
        print(grille[i])
    input("Press Enter to continue...")
